@extends('layouts.content')

@section('content')

			<div class="card">
				<div class="card-header">
					<h3><b>Users list</b></h3>
				</div>
				<div class="card">
					<div class="card-body">
						<table class="table table-hover">
							<thead>
								<tr>
									<th> name </th>
									<th> email </th>
									<th> created_at </th>
									<th> updated_at </th>
								</tr>
							</thead>
							<tbody>
							@foreach($users as $user)
								<tr>
									<td>{{ $user->name }}</td>
									<td>{{ $user->email }}</td>
									<td>{{ $user->created_at }}</td>
									<td>{{ $user->updated_at }}</td>
								</tr>
							@endforeach
							</tbody>
						</table>
						<div>
							@include('common.paginate', ['data' => $users])
						</div>
					</div>
				</div>
			</div>

@endsection
