@extends('layouts.content')

@section('content')
	<div class="card">
		<div class="card-header">
			<h3><b>Comments list</b></h3>
		</div>
		<div class="card">
			<div class="card-body">
				<table class="table table-hover">
					<thead>
						<tr>
							<th> title </th>
							<th> author </th>
							<th> created_at </th>
							<th> updated_at </th>
						</tr>
					</thead>
					<tbody>
					@foreach($comments as $comment)
						<tr>
							<td>{{ $comment->content }}</td>
							<td>{{ $comment->author }}</td>
							<td>{{ $comment->created_at }}</td>
							<td>{{ $comment->updated_at }}</td>
						</tr>
					@endforeach
					</tbody>
				</table>
				<div>
					@include('common.paginate', ['data' => $comments])
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
