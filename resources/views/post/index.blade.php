@extends('layouts.content')

@section('content')
			<div class="card">
				<div class="card-header">
					<h3><b>Posts list</b></h3>
				</div>
				<div class="card-body">
					<table class="table table-hover">
						<thead>
							<tr>
								<th> title </th>
								<th> author </th>
								<th> created_at </th>
								<th> updated_at </th>
							</tr>
						</thead>
						<tbody>
						@foreach($posts as $post)
							<tr>
								<td>{{ $post->title }}</td>
								<td>{{ $post->author }}</td>
								<td>{{ $post->created_at }}</td>
								<td>{{ $post->updated_at }}</td>
							</tr>
						@endforeach
						</tbody>
					</table>
					<div>
						@include('common.paginate', ['data' => $posts])
					</div>
				</div>
			</div>
@endsection