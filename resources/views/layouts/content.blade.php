@extends('layouts.app')
@section('body-content')
	<div class="container-fluid h-100">
		<div class="row h-100 py-3">
			<div class="col-sm-6 col-lg-3">
				<div class="card mb-3">
					<ul class="nav flex-sm-column justify-content-center"> 
						@include('layouts.menu')
					</ul>
				</div>
			</div>
			<div class="col">
				@yield('content')
			</div>
		</div>
	</div>
@endsection