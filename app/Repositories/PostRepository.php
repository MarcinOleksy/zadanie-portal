<?php

namespace App\Repositories;

use App\Repositories\Common\Repository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use App\Models\Post;

class PostRepository extends Repository 
{
    protected function model(): string
    {
        return Post::class;
    }

    public function getOneRandom()
    {
        return $this->query()->inRandomOrder()->limit(1)->first();
    }
}