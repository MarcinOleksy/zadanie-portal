<?php

namespace App\Repositories\Common;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

abstract class Repository {
	
    abstract protected function model():string;

    protected function query():Builder
    {
        return $this->makeModel()->newQuery();
    }

    protected function makeModel()
    {
        $class = $this->model();
        return new $class;
    }

    public function create(array $data): Model
    {
        return $this->query()->create($data);
    }

    public function paginate(): LengthAwarePaginator
    {
        $query = $this->query();

        return $query->paginate(10);
    }

    public function store(Model $model): bool
    {
        return $model->save();
    }

    public function updateByData(Model $model, array $data)
    {
        $model->fill($data);

        return $model;
    }

    public function newModel()
    {
        return $this->makeModel();
    }

    public function find(int $id)
    {
        $query = $this->query();

        return $query->find($id);
    }

    public function delete(Model $model): bool
    {
        return $model->delete();
    }

    public function findOrFail(int $id = null): Model
    {
        return $this->query()->findOrFail($id);
    }
}