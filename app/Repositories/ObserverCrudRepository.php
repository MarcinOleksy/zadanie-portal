<?php

namespace App\Repositories;

use App\Repositories\Common\Repository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use App\Models\ObserverCrud;
use Carbon\Carbon;

class ObserverCrudRepository extends Repository 
{
    protected function model(): string
    {
        return ObserverCrud::class;
    }

    public function getCount(Carbon $data)
    {
        return $this->query()->where('created_at', '>=', $data->toDateTimeString())->count();
    }
}