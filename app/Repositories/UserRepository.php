<?php

namespace App\Repositories;

use App\Repositories\Common\Repository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UserRepository extends Repository 
{
    protected function model(): string
    {
        return User::class;
    }

    public function create(array $data): Model
    {
        $data['password'] = Hash::make($data['password']);

        return $this->query()->create($data);
    }
}