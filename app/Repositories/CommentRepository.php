<?php

namespace App\Repositories;

use App\Repositories\Common\Repository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use App\Models\Comment;

class CommentRepository extends Repository 
{
    protected function model(): string
    {
        return Comment::class;
    }
}