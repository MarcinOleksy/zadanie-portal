<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repositories\ObserverCrudRepository;
use Carbon\Carbon;

class CountCrudPostAndCommentCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:countCRUD';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Count CRUD from post and comment model.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $data = Carbon::now()->subHours(13)->subMinutes(33);
    
        $observerCrudRepository = new ObserverCrudRepository();
        $countCrud = $observerCrudRepository->getCount($data);
 
        $this->info($countCrud);

        return 0;
    }
}
