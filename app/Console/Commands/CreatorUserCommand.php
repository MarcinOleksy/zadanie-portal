<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Validator;

class CreatorUserCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:createUser';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new user.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $validatedDate = [];

        $input = [
            'name' => [
                'question' => 'What your name?',
                'inputType' => "ask",
                'rules' => ["required"]
            ],
            'email' => [
                'question' => 'What your email?',
                'inputType' => 'ask',
                'rules' => ["required", "email", "max:60", "unique:users"]
            ],
            'password' => [
                'question' => 'What your password?',
                'inputType' => 'secret',
                'rules' => ['required', 'string', 'min:8']
            ],
        ];

        foreach($input as $key => $row)
            $validatedDate[$key] = $this->showQuestionAndValidateInput($key, $row);

        $userRepository = new UserRepository();
        $response = $userRepository->create($validatedDate);

        $this->info("Successfully created user.");

        return 0;
    }

    protected function showQuestionAndValidateInput($fieldName, $field)
    {
        $fieldValue = $this->{$field['inputType']}($field['question']);

        if($errorMessage = $this->validateInput($fieldName, $fieldValue, $field['rules']))
        {
            $this->error($errorMessage);

            return $this->showQuestionAndValidateInput($fieldName, $field);
        }

        return $fieldValue;
    }

    protected function validateInput($fieldName, $value, $rules)
    {
        $validator = Validator::make([
            $fieldName => $value
        ],[
            $fieldName => $rules
        ]);

        if($validator->fails())
            return $validator->errors()->first($fieldName);

        return null;
    }
}
