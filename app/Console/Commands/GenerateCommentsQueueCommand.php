<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repositories\ObserverCrudRepository;
use Carbon\Carbon;
use App\Jobs\GeneratorCommentQueueJob;

class GenerateCommentsQueueCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:runGeneratorCommentsQueue';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        GeneratorCommentQueueJob::dispatch()->onQueue('comments');

        return 0;
    }
}
