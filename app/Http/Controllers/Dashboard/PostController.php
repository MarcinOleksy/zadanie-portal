<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Repositories\PostRepository;

class PostController extends Controller
{
	public function index()
	{
		$postRepository = new PostRepository();
		$posts = $postRepository->paginate();

		return view('post.index', compact('posts'));
	}
}