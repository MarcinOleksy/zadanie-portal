<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;

class UserController extends Controller
{
	public function index()
	{
		$userRepository = new UserRepository();
		$users = $userRepository->paginate();

		return view('user.index', compact('users'));
	}
}