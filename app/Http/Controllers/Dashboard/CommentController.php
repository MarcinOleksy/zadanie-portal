<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\CommentRepository;

class CommentController extends Controller
{
	public function index()
	{
		$commentRepository = new CommentRepository();
		$comments = $commentRepository->paginate();

		return view('comment.index', compact('comments'));
	}
}