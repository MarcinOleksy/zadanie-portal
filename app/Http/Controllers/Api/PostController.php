<?php 

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\PostRepository;
use App\Http\Resources\PostResource;
use App\Http\Requests\PostApiRequest;
use \Illuminate\Database\Eloquent\ModelNotFoundException;

class PostController extends Controller
{
	public function index()
	{
		$postRepository = new PostRepository();
		$posts = $postRepository->paginate();

		return new PostResource($posts);
	}

	public function show(int $id)
	{
		$postRepository = new PostRepository();

		try{
           	$post = $postRepository->findOrFail($id);
		} catch (ModelNotFoundException $ex) {
			return $this->responseMessage(false, 'Record not found', 404);
		}

		return new PostResource($post);
	}

	public function store(PostApiRequest $request)
	{
		$postRepository = new PostRepository();
		$postRepository->create($request->all());

		return $this->responseMessage(true, 'Successfully created post.', 200);
	}

	public function edit(PostApiRequest $request, int $id)
	{
		$postRepository = new PostRepository();

		try{
           	$post = $postRepository->findOrFail($id);
		} catch (ModelNotFoundException $ex) {
			return $this->responseMessage(false, 'Record not found', 404);
		}

		$postRepository->updateByData($post, $request->all());

		$post->save();

		return $this->responseMessage(true, 'Successfully edited post.', 200);
	}

	public function delete(int $id)
	{
		$postRepository = new PostRepository();

		try{
           	$post = $postRepository->findOrFail($id);
		} catch (ModelNotFoundException $ex) {
			return $this->responseMessage(false, 'Record not found', 404);
		}

		$postRepository->delete($post);

		return $this->responseMessage(true, 'Successfully deleted post.', 200);

	}

	protected function responseMessage(bool $success, string $message, int $httpCode)
	{
		return response()->json([
				'success'   => $success,
                'message' => $message,
            ], $httpCode);
	}
}