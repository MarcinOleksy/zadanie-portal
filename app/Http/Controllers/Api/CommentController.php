<?php 

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\CommentRepository;
use App\Http\Resources\CommentResource;
use App\Http\Requests\CommentApiRequest;
use \Illuminate\Database\Eloquent\ModelNotFoundException;

class CommentController extends Controller
{
	public function index()
	{
		$commentRepository = new CommentRepository();
		$comments = $commentRepository->paginate();

		return new CommentResource($comments);
	}

	public function show(int $id)
	{
		$commentRepository = new CommentRepository();

		try{
           	$comment = $commentRepository->findOrFail($id);
		} catch (ModelNotFoundException $ex) {
			return $this->responseMessage(false, 'Record not found', 404);
		}

		return new CommentResource($comment);
	}

	public function store(CommentApiRequest $request)
	{
		$commentRepository = new CommentRepository();
		$commentRepository->create($request->all());

		return $this->responseMessage(true, 'Successfully created comment.', 200);
	}

	public function edit(CommentApiRequest $request, int $id)
	{
		$commentRepository = new CommentRepository();

		try{
           	$comment = $commentRepository->findOrFail($id);
		} catch (ModelNotFoundException $ex) {
			return $this->responseMessage(false, 'Record not found', 404);
		}

		$commentRepository->updateByData($comment, $request->all());

		$comment->save();

		return $this->responseMessage(true, 'Successfully edited comment.', 200);
	}

	public function delete(int $id)
	{
		$commentRepository = new CommentRepository();
		
		try{
           	$comment = $commentRepository->findOrFail($id);
		} catch (ModelNotFoundException $ex) {
			return $this->responseMessage(false, 'Record not found', 404);
		}

		$commentRepository->delete($comment);

		return $this->responseMessage(true, 'Successfully deleted comment.', 200);
	}
}