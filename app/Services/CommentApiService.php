<?php

namespace App\Services;
use Illuminate\Support\Facades\Http;

class CommentApiService
{
	public function index()
	{
		return Http::get(route('api.comment.index'))->json();
	}

	public function show(int $id)
	{
		return Http::get(route('api.comment.index',['id' => $id]))->json();
	}

	public function store(array $data)
	{
		return Http::post(route('api.comment.store'), $data)->json();
	}

	public function edit(int $id, array $data)
	{
		return Http::put(route('api.comment.edit',['id' => $id]), $data)->json();
	}

	public function delete()
	{
		return Http::delete(route('api.comment.delete',['id' => $id]))->json();
	}
}