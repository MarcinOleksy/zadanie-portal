<?php

namespace App\Services;

use App\Repositories\ObserverCrudRepository;
use Carbon\Carbon;

use App\Services\CommentApiService;
use App\Repositories\PostRepository;
use App\Models\Post;

class CreatorCommentForRandomPostService
{
	public function create()
	{
		$post = $this->getRandomPost();

		$data = $this->generateComment($post);

		return $this->insertComment($data);
	}

	protected function getRandomPost()
	{
		$postRepository = new PostRepository();
		return $postRepository->getOneRandom();
	}

	protected function generateComment(Post $post)
	{
		return [
			'post_id' => $post->id,
			'content' => 'tak',
			'author' => 'job_tak'
		];
	}

	protected function insertComment(array $data)
	{
		$commentApiService = new CommentApiService();

		return $commentApiService->store($data);
	}
}