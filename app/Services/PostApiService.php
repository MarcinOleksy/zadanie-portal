<?php

namespace App\Services;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Client\RequestException;

class PostApiService
{
	public function index()
	{
		return Http::get(route('api.post.index'))->json();
	}

	public function show(int $id)
	{
		return Http::get(route('api.post.index',['id' => $id]))->json();
	}

	public function store(array $data)
	{
		return Http::post(route('api.post.store'), $data)->json();
	}

	public function edit(int $id, array $data)
	{
		return Http::put(route('api.post.edit',['id' => $id]), $data)->json();
	}

	public function delete()
	{
		return Http::delete(route('api.post.delete',['id' => $id]))->json();
	}
}