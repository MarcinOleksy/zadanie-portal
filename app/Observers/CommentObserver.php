<?php

namespace App\Observers;

use App\Models\Comment;
use App\Repositories\ObserverCrudRepository;

class CommentObserver
{
    protected $model_typ = "comment";
    
    /**
     * Handle the Comment "created" event.
     *
     * @param  \App\Models\Comment  $comment
     * @return void
     */
    public function created(Comment $comment)
    {
        $observerCrudRepository = new ObserverCrudRepository();

        $data = [
            'model' => $this->model_typ,
            'crud_type' => "create"
        ];

        $observerCrudRepository->create($data);
    }

    /**
     * Handle the Comment "updated" event.
     *
     * @param  \App\Models\Comment  $comment
     * @return void
     */
    public function updated(Comment $comment)
    {

        $observerCrudRepository = new ObserverCrudRepository();

        $data = [
            'model' => $this->model_typ,
            'crud_type' => "update"
        ];

        $observerCrudRepository->create($data);
    }

    /**
     * Handle the Comment "deleted" event.
     *
     * @param  \App\Models\Comment  $comment
     * @return void
     */
    public function deleted(Comment $comment)
    {
        $observerCrudRepository = new ObserverCrudRepository();

        $data = [
            'model' => $this->model_typ,
            'crud_type' => "delete"
        ];

        $observerCrudRepository->create($data);
    }

    /**
     * Handle the Comment "restored" event.
     *
     * @param  \App\Models\Comment  $comment
     * @return void
     */
    public function restored(Comment $comment)
    {
        //
    }

    /**
     * Handle the Comment "force deleted" event.
     *
     * @param  \App\Models\Comment  $comment
     * @return void
     */
    public function forceDeleted(Comment $comment)
    {
        //
    }
}
