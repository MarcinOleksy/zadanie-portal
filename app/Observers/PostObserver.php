<?php

namespace App\Observers;

use App\Models\Post;
use App\Repositories\ObserverCrudRepository;

class PostObserver
{
    protected $model_typ = 'post';

    /**
     * Handle the Post "created" event.
     *
     * @param  \App\Models\Post  $post
     * @return void
     */
    public function created(Post $post)
    {
        $observerCrudRepository = new ObserverCrudRepository();

        $data = [
            'model' => $this->model_typ,
            'crud_type' => 'create'
        ];

        $observerCrudRepository->create($data);
    }

    /**
     * Handle the Post "updated" event.
     *
     * @param  \App\Models\Post  $post
     * @return void
     */
    public function updated(Post $post)
    {
        $observerCrudRepository = new ObserverCrudRepository();

        $data = [
            'model' => $this->model_typ,
            'crud_type' => 'update'
        ];

        $observerCrudRepository->create($data);
    }

    /**
     * Handle the Post "deleted" event.
     *
     * @param  \App\Models\Post  $post
     * @return void
     */
    public function deleted(Post $post)
    {
        $observerCrudRepository = new ObserverCrudRepository();

        $data = [
            'model' => $this->model_typ,
            'crud_type' => 'delete'
        ];

        $observerCrudRepository->create($data);
    }

    /**
     * Handle the Post "restored" event.
     *
     * @param  \App\Models\Post  $post
     * @return void
     */
    public function restored(Post $post)
    {
        //
    }

    /**
     * Handle the Post "force deleted" event.
     *
     * @param  \App\Models\Post  $post
     * @return void
     */
    public function forceDeleted(Post $post)
    {
        //
    }
}
