<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model {	
	protected $table = "comments";

	protected $fillable = [
		"post_id",
		"content",
		"author",
		"created_at",
		"updated_at"
	];

	protected $timestamp = true;
}