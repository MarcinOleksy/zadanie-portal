<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ObserverCrud extends Model {	
	protected $table = "observer_crud";

	protected $fillable = [
		"model",
		"crud_type",
		"created_at"
	];

	protected $timestamp = true;
	const UPDATED_AT  = null;	

}