<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\PostController;
use App\Http\Controllers\Api\CommentController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/posts', [PostController::class, 'index'])->name('api.post.index');
Route::post('/post/store', [PostController::class, 'store'])->name('api.post.store');
Route::get('/post/{id}', [PostController::class, 'show'])->name('api.post.show');
Route::put('/post/edit/{id}', [PostController::class, 'edit'])->name('api.post.edit');
Route::delete('/post/delete/{id}', [PostController::class, 'delete'])->name('api.post.delete');

Route::get('/comments', [CommentController::class, 'index'])->name('api.comment.index');
Route::post('/comment/store', [CommentController::class, 'store'])->name('api.comment.store');
Route::get('/comment/{id}', [CommentController::class, 'show'])->name('api.comment.show');
Route::put('/comment/edit/{id}', [CommentController::class, 'edit'])->name('api.comment.edit');
Route::delete('/comment/delete/{id}', [CommentController::class, 'delete'])->name('api.comment.delete');
